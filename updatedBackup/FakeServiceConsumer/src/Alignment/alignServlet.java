package Alignment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

@WebServlet(name = "alignServlet", urlPatterns = { "/res" })
public class alignServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ArrayList<String> list = (ArrayList<String>) request.getSession()
				.getAttribute("un_rdg");

		ArrayList<String> rdg = (ArrayList<String>) request.getSession()
				.getAttribute("rdg");
		ArrayList<String> onto = (ArrayList<String>) request.getSession()
				.getAttribute("onto");

		String name = (String) request.getSession().getAttribute("name");

		for (int i = 0; i < rdg.size(); i++) {

			String str = request.getParameter(rdg.get(i));
			if (!str.equals("SEL")) {

				rdg.set(i, rdg.get(i));
				onto.set(i, str);
			}

		}

		for (int i = 0; i < list.size(); i++) {

			String str = request.getParameter(list.get(i));
			if (!str.equals("SEL")) {
				rdg.add(list.get(i));
				onto.add(str);
			}

		}

		request.getSession().setAttribute("RDG", rdg);
		request.getSession().setAttribute("ONTO", onto);

		ArrayList<String> csv = new ArrayList<String>();

		for (int i = 0; i < rdg.size(); i++) {

			String st = onto.get(i) + "," + rdg.get(i);
			csv.add(st);

		}

		File filename = new File("D:\\EE\\" + name);
		

		try {
			FileWriter fw = new FileWriter(filename);
			Writer output = new BufferedWriter(fw);
			for (int i = 0; i < csv.size(); i++) {

				output.write(csv.get(i).toString() + "\n");
			}
			output.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "can not write");
		}
		
		
		

		RequestDispatcher rd = request.getRequestDispatcher("/BookingForm.jsp");
		rd.forward(request, response);

	}

}
