package endPoint;

import java.io.IOException;
import java.util.Random;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;

@WebServlet(name = "Servlet", urlPatterns = { "/result" })
public class Servlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String buildTable = null;
	String returnTable = null;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String username = request.getParameter("name");
		int people = Integer.parseInt(request.getParameter("people"), 10);
		int bedrooms = Integer.parseInt(request.getParameter("bedrooms"), 10);
		int lake_distance = Integer.parseInt(request.getParameter("lake_distance"), 10);
		String city = request.getParameter("city");
		int city_distance = Integer.parseInt(request.getParameter("city_disance"), 10);
		String startdate = request.getParameter("startdate");
		int days = Integer.parseInt(request.getParameter("days"), 10);
		int flex = Integer.parseInt(request.getParameter("flex"), 10);

		String returnTable = null;
		String error = null;
		String book1 = null;
		String book2 = null;
		String book3 = null;
		String book4 = null;
		int randomNum = 0;
		int IN;
		int OUT;

		bookingdates obju = new bookingdates();
		obju.datecreator(startdate, days);
		int S = obju.StartDate;
		int E = obju.StartDate + days - 1;

		QueryStringBuilder obj1 = new QueryStringBuilder();
		obj1.StringBuilder(city, people, bedrooms, city_distance, lake_distance, S, E, flex);
		Query query = obj1.query;
		OntModel model1 = obj1.model1;

		if (query.isSelectType()) {

			QueryExecution qe = QueryExecutionFactory.create(query, model1);
			com.hp.hpl.jena.query.ResultSet results = qe.execSelect();

			if (!results.hasNext()) {
				error = "No Result found According your search, kindly change input parameter";

			}

			else {

				int max = 10000000;
				int min = 5000000;
				Random rand = new Random();
				randomNum = rand.nextInt((max - min) + 1) + min;
				request.setAttribute("bookingnumber", randomNum);
				request.setAttribute("user", username);

				returnTable = "<table border=1>";
				returnTable = returnTable + "<tr>";

				while (results.hasNext()) {

					// ResultSetFormatter.out(System.out, results, query);
					QuerySolution row = results.nextSolution();
					String x = row.getLiteral("start").getString();
					String y = row.getLiteral("end").getString();
					int start = Integer.parseInt(x);
					int end = Integer.parseInt(y);

					returnTable = returnTable + "<td>" + " Cottege Name:  " + row.getLiteral("Name").getString() + "</td>";
					returnTable = returnTable + "<td>" + " URL  " + row.getLiteral("url").getString() + "</td>";
					returnTable = returnTable + "<td>" + "Number of Beds " + row.getLiteral("AmountOfBeds").getString() + "</td>";
					returnTable = returnTable + "<td>" + "Amount of People " + row.getLiteral("people").getString() + "</td>";
					returnTable = returnTable + "<td>" + "Address " + row.getLiteral("address").getString() + "</td>";
					returnTable = returnTable + "<td>" + "Nearby City " + row.getLiteral("cityname").getString() + "</td>";
					returnTable = returnTable + "<td>" + " Distance from City " + row.getLiteral("city_distance").getString() + " Meters" + "</td>";
					returnTable = returnTable + "<td>" + "Distance from Lake " + row.getLiteral("lake_distance").getString() + " Meters" + "</td>";

					if (flex >= 1) {

						if (S - flex >= start && E - flex <= end) {

							IN = S - flex;
							OUT = E - flex;

							Day2Date obj = new Day2Date();

							obj.Day(IN);
							String Checkin = obj.date;

							obj.Day(OUT);
							String Checkout = obj.date;

							book1 = (" Booking period = " + Checkin + " to " + Checkout);

						} else {
							book1 = " ";
						}

						if (S >= start && E <= end) {

							IN = S;
							OUT = E;

							Day2Date obj = new Day2Date();

							obj.Day(IN);
							String Checkin = obj.date;

							obj.Day(OUT);
							String Checkout = obj.date;

							book2 = (" Booking period = " + Checkin + " to " + Checkout);

						} else {
							book2 = " ";
						}

						if (S + flex >= start && E + flex <= end) {

							IN = S + flex;
							OUT = E + flex;

							Day2Date obj = new Day2Date();

							obj.Day(IN);
							String Checkin = obj.date;

							obj.Day(OUT);
							String Checkout = obj.date;

							book3 = (" Booking period = " + Checkin + " to " + Checkout);
						} else {
							book3 = " ";
						}

						returnTable = returnTable + "<td>" + book1 + book2 + book3 + "</td>";

					}

					if (flex == 0) {

						if (S >= start && E <= end) {

							IN = S;
							OUT = E;

							Day2Date obj = new Day2Date();

							obj.Day(IN);
							String Checkin = obj.date;

							obj.Day(OUT);
							String Checkout = obj.date;

							book4 = (" Booking period = " + Checkin + " to " + Checkout);

							System.out.println(" Booking period =  " + Checkin + " to " + Checkout + "\n");

						} else {
							book4 = " ";
						}

						returnTable = returnTable + "<td>" + book4 + "</td>";

					}

					returnTable = returnTable + "<tr>";

					// ResultSetFormatter.out(System.out, results, query);

				}

			}

			qe.close();

		}

		request.setAttribute("result", returnTable);
		request.setAttribute("error", error);

		RequestDispatcher rd = request.getRequestDispatcher("/result.jsp");
		rd.forward(request, response);

	}

}
