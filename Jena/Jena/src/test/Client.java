package test;

import java.io.FileInputStream;
import java.io.IOException;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.InfModel;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.reasoner.Reasoner;
import com.hp.hpl.jena.reasoner.rulesys.GenericRuleReasoner;
import com.hp.hpl.jena.reasoner.rulesys.Rule;
import com.hp.hpl.jena.util.FileManager;

public class Client {
	
	
	public Statement stmt=null;
	public boolean ask;
	public String querytype;
    public ResultSet results;
	
	
	public void operation ( String URL, String rules, String QUERY ) throws IOException {
		 
		 
	     // collecting raw data
	        Model rawData = FileManager.get().loadModel(URL);

	         
	        //creating model using reasoner
	        Reasoner reasoner = new GenericRuleReasoner(Rule.parseRules(rules));
	        InfModel model1 = ModelFactory.createInfModel(reasoner, rawData);
	        
	        //getting query
	        String queryString = QUERY;
	        Query query = QueryFactory.create(queryString);
	        
	        
	        
	        if (query.isSelectType()){
	        	querytype = "select";
	        	QueryExecution qe = QueryExecutionFactory.create(query, model1);
	        	results =  qe.execSelect(); 
	            ResultSetFormatter.out(System.out, results, query);
	        }
	       
	       
	        else if (query.isConstructType()){
	        	querytype = "construct";
	        	QueryExecution qe = QueryExecutionFactory.create(query, model1);
	            Model resultModel = qe.execConstruct() ;
	            StmtIterator iter = resultModel.listStatements();
		         while (iter.hasNext()) {
		        	 stmt = iter.next();}
		         stmt.getPredicate().toString();
		         stmt.getSubject().toString();
	            resultModel.write(System.out, "TURTLE");          
	        }
	   
	        else if (query.isDescribeType()){
	        	querytype = "describe";
	        	QueryExecution qe = QueryExecutionFactory.create(query, model1);
	        	Model resultModel = qe.execDescribe() ;
	        	StmtIterator iter = resultModel.listStatements();
		         while (iter.hasNext()) {  	 
		        	 stmt = iter.next();}
	            resultModel.write(System.out, "TURTLE");
	        	
	        }
	        
	        else if (query.isAskType()){
	        	querytype = "ask";
	        	QueryExecution qe = QueryExecutionFactory.create(query, model1);
	        	 ask = qe.execAsk() ;
	        	System.out.println("Result is"+ "   "+ ask);
	            
	        	
	        }
		 
		 
	 }

}
