package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

public class DateChecking {


public String[] DateArray;
public int res;
	
public void datechecking( String S, String E, String city){
	
	String CITY = "\"" + city + "\"";
	
	ArrayList<String> date = new ArrayList<String>();
	
	String SOURCE = "http://users.jyu.fi/~syibkhan/15.rdf";
	
	OntModel model1 = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM_MICRO_RULE_INF);
    // read the RDF/XML file
    model1.read( SOURCE, "RDF/XML" ); 
    
    
    
    String queryString = "PREFIX g: <http://www.semanticweb.org/ibrahim/ontologies/2015/2/untitled-ontology-126#>" +
"PREFIX owl:<http://www.w3.org/2002/07/owl#>"+
"PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
"PREFIX xml:<http://www.w3.org/XML/1998/namespace>"+
"PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>"+
"PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>"+
"SELECT DISTINCT ?cottege ?date ?City "+ 
"WHERE {"+
"?cottege  g:hasName ?Name."+
"?cottege  g:isSituatedIn ?City;"+
"g:hasBookingDate  ?date."+
"FILTER (?City = " + CITY +"^^xsd:string).\n " +

"}" ;
    		
    		
    Query query = QueryFactory.create(queryString);
    
    
    if (query.isSelectType()){
    	
    	QueryExecution qe = QueryExecutionFactory.create(query, model1);
        com.hp.hpl.jena.query.ResultSet results =  qe.execSelect(); 
        while (results.hasNext()) {
            QuerySolution row=results.nextSolution();
            
        
           
            String x = row.getLiteral("date").getString();
            date.add(x);
            
            
  
//            System.out.println( row.getLiteral("Freedate").getString());
//            
            
        
        }
        
        DateArray = date.toArray(new String[0]);
        System.out.println("date test"+ Arrays.toString(DateArray));
        
        
        Date obj50 = new Date();
        obj50.tester(S, E, DateArray );
        res = obj50.flag;
        
        
        
        
//        System.out.println(Arrays.toString(DateArray));
//        ResultSetFormatter.out(System.out, results, query);
        qe.close();
//    	
    }



			



	
}

}

